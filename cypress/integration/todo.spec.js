const todoData = require('./../../cypress/fixtures/todo.json')
let Active=0, Completed=0
describe('Validate ToDo page',()=>{
    it('Validate search in ToDo Page',()=>{
        cy.visit('https://todomvc.com/examples/react/#/')
    })
    it('Enter Data',()=>{
        todoData.forEach(element => {
            cy.get('.new-todo').type(element.DataEntered).type('{enter}')
        });

    })
    it('Check Status of each element',()=>{
        let i=0,j=0;
        todoData.forEach(element => {
            
        if(element.Status=='Active'){
            cy.log(element.DataEntered+' is Active')
            Active = Active+1
            
           
        }else if(element.Status=='Completed'){
            cy.log(element.DataEntered+' is Completed')
            cy.get('.todo-list .toggle').eq(i).click()          
            Completed = Completed+1
            
        }
        i=i+1;
        });
    })
    it('Validate Active and Completed Tasks',()=>{
        cy.get('a[href="#/active"]').click()
        cy.get('.todo-list>li').should('have.length',Active)
        cy.get('a[href="#/completed"]').click()
        cy.get('.todo-list>li').should('have.length',Completed)
        cy.get('a[href="#/"]').click()
        cy.get('.todo-list>li').should('have.length',Active+Completed)
        // cy.log(Active + ' '+Completed)
    })
})