// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const addContext = require("mochawesome/addContext");

let LOCAL_STORAGE_MEMORY = {};
let SESSION_STORAGE_MEMORY = {};

Cypress.Commands.overwrite('log', (subject, message) => cy.task('log', message));

Cypress.Commands.add("saveLocalStorageCache", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorageCache", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});

Cypress.Commands.add("saveSessionStorageCache", () => {
  Object.keys(sessionStorage).forEach(key => {
    SESSION_STORAGE_MEMORY[key] = sessionStorage[key];
  });
});

Cypress.Commands.add("restoreSessionStorageCache", () => {
  Object.keys(SESSION_STORAGE_MEMORY).forEach(key => {
    sessionStorage.setItem(key, SESSION_STORAGE_MEMORY[key]);
  });
});

Cypress.Commands.add("clearLocalStorageCache", () => {
  localStorage.clear();
  LOCAL_STORAGE_MEMORY = {};
});

Cypress.Commands.add('setLocalStorageForFlightResultsPage', () => {
  localStorage.setItem('postData905804e3-3f28-4c51-b12a-ac379c0ce32e',
    '{"bestFare":"BE","action":"findFlights","destinationAirportRadius":{"unit":"MI","measure":100},"deltaOnlySearch":false,"meetingEventCode":"","originAirportRadius":{"unit":"MI","measure":100},"passengers":[{"type":"ADT","count":1}],"searchType":"search","segments":[{"returnDate":"2021-04-30","departureDate":"2021-04-22","destination":"MSP","origin":"ATL"}],"shopType":"MONEY","tripType":"ROUND_TRIP","priceType":"Revenue","priceSchedule":"REVENUE","awardTravel":false,"refundableFlightsOnly":false,"nonstopFlightsOnly":false,"datesFlexible":false,"flexCalendar":false,"flexAirport":false,"upgradeRequest":false,"corporateSMTravelType":"Personal","pageName":"FLIGHT_SEARCH","cacheKey":"905804e3-3f28-4c51-b12a-ac379c0ce32e","requestPageNum":"1","selectedSolutions":[{"sliceIndex":1}],"actionType":"","initialSearchBy":{"fareFamily":"BE","meetingEventCode":"","refundable":false,"flexAirport":false,"flexDate":false,"flexDaysWeeks":""}}')
})

Cypress.Commands.add('ccrFeedbackComplaints_launch', () => {
  cy.visit('/us/en/need-help/overview');
  cy.get('#ccrForm').should('be.visible').click();
});

Cypress.Commands.add('clearAdditionalBrowserSessionCookies', () => {
  cy.clearCookie('clflg')
  cy.clearCookie('prfId')
});

Cypress.Commands.add('CloseFixedAdvisoryBanner', () => {
  cy.get('body').then((body) => {
    if (body.find('.alert-advisory-main-container:visible').length > 0) {
      cy.get('.alert-advisory-main-container .advisory-close-icon').click({ force: true });
    }
  });
})

Cypress.Commands.add('validateConfirmationSpinner', () => {
  cy.url().then((url) => {
    if (url.includes('://dvl2')) {
      cy.get('.interstitialpageview .spinner-loading .loader-holder', { timeout: 1000 }).should('be.visible')
    }
  })
})

Cypress.Commands.add('addTestContext', (title, value) => {
  cy.once('test:after:run', test => addContext({ test }, { title, value }))
})

Cypress.Commands.add('PrintCurrentUrl', () => {
  cy.url().then((url) => {
    cy.log(url)
    cy.addTestContext('Current Url is :', url)
  })
})

Cypress.Commands.add('addTealeafSessionAsTestContext', () => {
  cy.getCookie('TLTSID').then((cookie) => {
    cy.addTestContext('Tealeaf Session ID:', cookie.value)
    // cy.once('test:after:run', test => addContext({ test }, 'Tealeaf Session ID: ' + cookie.value))
  })
})

Cypress.Commands.add('setLocalStorageItem', (attribute, value) => {
  cy.get('body').then((body) => {
    window.localStorage.setItem(attribute, value)
  });
})

Cypress.Commands.add('assignCypressTestEnvironmentFromBaseURL', () => {
  cy.url().then((env) => {
    if ((env.includes('://www.')) || (env.includes('://prd'))) {
      Cypress.env('TestEnvironment', 'PROD')
    } else if ((env.includes('://si.')) || (env.includes('://stg'))) {
      Cypress.env('TestEnvironment', 'STG')
    } else if ((env.includes('://st.')) || (env.includes('://qat'))) {
      Cypress.env('TestEnvironment', 'QAT')
    } else if (env.includes('://dvl')) {
      Cypress.env('TestEnvironment', 'DVL')
    }
  })
})

Cypress.Commands.add('getDTMPOrder', () => {
  let jsonCustomerObj;
  cy.readFile('cypress/fixtures/ContentManagement/RefundPortal/testData.json').then((testData) => {
    cy.url().then((env) => {
      if ((env.includes('://dvl'))) {
        jsonCustomerObj = testData.dvl_input_searchByCustomerDetails;
      } else if ((env.includes('://st.')) || (env.includes('://qat'))) {
        jsonCustomerObj = testData.st_input_searchByCustomerDetails;
      } else if ((env.includes('://si.')) || (env.includes('://stg'))) {
        jsonCustomerObj = testData.si_input_searchByCustomerDetails;
      } else {
        jsonCustomerObj = testData.prod_input_searchByCustomerDetails;
      }
      return jsonCustomerObj;
    })
  })
})


// SEARCH AND PURCHASE - CHECKOUT

Cypress.Commands.add('validateExpressCKOPageProperties', () => {
  cy.request('/content/www/en_US/rsb/checkout-error-messages.contfragtdata.json').then((response) => {
    let featureFlagAEMJSON = response.body;
    if (typeof featureFlagAEMJSON === 'string') {
      featureFlagAEMJSON = JSON.parse(featureFlagAEMJSON)
    }
    let enableMergeExpCKOSwitch = featureFlagAEMJSON["featureFlags"]["generalFeatureFlags"]["enableMergeExpCKO"]
    enableMergeExpCKOSwitch = (enableMergeExpCKOSwitch === 'true') // enableMergeExpCKOSwitch is coming as String
    cy.log('Flag value is ' + enableMergeExpCKOSwitch)
    if (enableMergeExpCKOSwitch) {   //  New URL format expected
      cy.get('.review-pay-view', { timeout: 60000, interval: 5000 }).should('be.visible')
      cy.get('h1').should('contain.text', 'Express Checkout', { timeout: 10000 })
      cy.url().should('include', '/complete-purchase/express-cko').should('include', '?cacheKeySuffix=').should('include', '&cartId=')
    } else { //  Old URL format expected
      cy.get('.express-checkout-view', { timeout: 60000, interval: 5000 }).should('be.visible')
      cy.get('h1').should('contain.text', 'Express Checkout', { timeout: 10000 })
      cy.url().should('include', '/complete-purchase/express-checkout').should('include', '?cacheKeySuffix=').should('include', '&cartId=')
    }
  })
})
Cypress.Commands.add('validateTripSummaryPageProperties', () => {
  cy.get('.trip-summary-view', { timeout: 60000, interval: 5000 }).should('be.visible')
  cy.get('h1').should('contain.text', 'Trip Summary', { timeout: 10000 })
  cy.url().should('include', '/complete-purchase/trip-summary').should('include', '?cacheKeySuffix=').should('include', '&cartId=')
})
Cypress.Commands.add('validateReviewPayPageProperties', () => {
  cy.get('.review-pay-view', { timeout: 60000, interval: 5000 }).should('be.visible')
  cy.get('h1').should('contain.text', 'Review and Pay', { timeout: 10000 })
  cy.url().should('include', '/complete-purchase/review-pay').should('include', '?cacheKeySuffix=').should('include', '&cartId=')
})